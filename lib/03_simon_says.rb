def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word,number=2)
  ([word]*number).join(" ")
end

def start_of_word(word, number)
  puts word
  word[0...number]
end

def first_word(sentence)
  sentence.split[0]
end

def titleize(title)
  exceptions = ["and","over","the"]
  final_title = title.split.map{|word| exceptions.include?(word) ? (word) : word.capitalize}.join(" ")
  final_title[0].upcase+final_title[1..-1]
end
