def add(a,b)
  a+b
end

def subtract(a,b)
  a-b
end

def sum(a)
  return 0 if a.empty?
  a.reduce(:+)
end

def multiply(a)
  a.reduce(:*)
end

def power(a,b)
  a**b
end

def factorial(a)
  (1..a).inject(:*) || 1
end
