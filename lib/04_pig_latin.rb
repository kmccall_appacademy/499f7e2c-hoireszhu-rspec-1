def translate(sentence)
  sentence.split(" ").map{|word| pig_latin(word)}.join(" ")
end

def pig_latin(word)
  alpha = ('a'..'z').to_a
  vowels = ["a","e","i","o","u"]
  consonants = alpha - vowels

  return "#{word}ay" if vowels.include?(word[0])

  if word[0..1] == "qu"
    return word[2..-1] + "quay"
  elsif word[0..2] == "sch"
    return word[3..-1] + "schay"
  elsif word[0..2] == "squ"
    return word[3..-1] + "squay"
  elsif consonants.include?(word[0]) && consonants.include?(word[1]) && consonants.include?(word[2])
    return word[3..-1] + word[0..2] + "ay"
  elsif consonants.include?(word[0]) && consonants.include?(word[1])
    return word[2..-1] + word[0..1] + "ay"
  elsif consonants.include?(word[0])
    return word[1..-1] + word[0] + "ay"
  end
end
